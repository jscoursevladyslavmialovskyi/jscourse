import MyOnClick from './modules/OnClickFunction.js';
//Get Buttons
var AlertPopupButton = document.getElementById("AlertPopupButton");
var ConfirmPopupButton = document.getElementById("ConfirmPopupButton");
var PromptPopupButton = document.getElementById("PromptPopupButton");
var ConsoleLogButton = document.getElementById("ConsoleLogButton");
//Change OnClick

AlertPopupButton.onclick = MyOnClick;
ConfirmPopupButton.onclick = MyOnClick;
PromptPopupButton.onclick = MyOnClick;
ConsoleLogButton.onclick = MyOnClick;