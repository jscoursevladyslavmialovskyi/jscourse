function MyOnClick(event) {
    switch (event.srcElement.id) {
        case "AlertPopupButton":
            var Input = document.getElementById("AlertPopupInput");
            alert(Input.value)
            break;
        case "ConfirmPopupButton":
            var Input = document.getElementById("ConfirmPopupInput");
            confirm(Input.value);
            break;
        case "PromptPopupButton":
            var Input = document.getElementById("PromptPopupInput");
            prompt(Input.value);
            break;
        case "ConsoleLogButton":
            var Input = document.getElementById("ConsoleLogInput");
            alert("Check your console");
            console.log(Input.value);
            break;
    }
}

export default MyOnClick;